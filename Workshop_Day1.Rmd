---
title: "R Workshop: Day 1"
number-sections: true
format: 
  html:
    toc: true
editor_options: 
  markdown: 
    wrap: false
  chunk_output_type: inline
---


```{r child = '01_Basics.Rmd'}
```

```{r child = '02_Data_Wrangling.Rmd'}
```

```{r child = '03_Data_Tidying.Rmd'}
```

```{r child = '04_Plotting.Rmd'}
```